#include "players.h"
#include <iostream>

using std::cout; using std::endl;

int main() {

   // --------------------------------------
   // This part may change during evaluation
   Warrior t1_p1("Bob", 3);
   Warrior t1_p2("Hubert", 2);
   
   Warrior t2_p1("Hans", 5);
   Healer  t2_p2("Luigi", 2);
   // --------------------------------------
   cout << "Players are :" << endl
        << " -- " << t1_p1.to_string() << " (team 1) " << endl
        << " -- " << t1_p2.to_string() << " (team 1) " << endl
        << " -- " << t2_p1.to_string() << " (team 2) " << endl
        << " -- " << t2_p2.to_string() << " (team 2) " << endl;

   cout << "Game starts, number of players : " << Character::nPlayers() << endl;
   for (size_t i = 0; i < 6; i++) {
      // Player 1 from team 1 attacks
      if (t2_p1.isAlive()) {
         t1_p1.attacks(t2_p1);
      } else {
         t1_p1.attacks(t2_p2);
      }
      // Player 1 from team 2 attacks
      if (t1_p1.isAlive()) {
         t2_p1.attacks(t1_p1);
      } else {
         t2_p1.attacks(t1_p2);
      }
      // Player 2 from team 1 attacks
      if (t2_p1.isAlive()) {
         t1_p2.attacks(t2_p1);
      } else {
         t1_p2.attacks(t2_p2);
      }
      // Player 2 from team 2 heals
      if (t2_p1.isAlive()) {
         t2_p2.heals(t2_p1);
      } else {
         t2_p2.heals(t2_p2);
      }
   }
   cout << "End of the game, players left : " << Character::nPlayers() << endl;
   return 0;
}
