class Character {

protected:
   // Name of the character
   string name;
   // Maximum (and initial) number of HP
   size_t maxHP;
   // Current number of HP
   size_t hitPoints;

public:
   // Base constructor
   Character(string name);

   static size_t nPlayers();  // Return the number of players that are not dead (ie HP=0)
   string to_string() const;  // Convert into a string
   bool isAlive() const;  // Return if the player is still alive (ie HP>0)
   void changeHP(int delta);  // Change the number of HP of the character (positive of negative delta)
};

class Warrior: public Character {
private:
   // Number of HP remove after an attack
   size_t attackPts;

public:
// Base constructor
   Warrior(string name, size_t attackPts);

   string to_string() const;  // Convert into a string
   void attacks(Character& other);  // Attack an other character
};

class Healer: public Character {
private:
   // Number of HP added after heal
   size_t healPts;

public:
// Base constructor
   Healer(string name, size_t healPts);

   string to_string() const;  // Convert into a string
   void heals(Character& other);  // Heals an other character
};
