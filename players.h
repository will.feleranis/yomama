#ifndef players_h
#define players_h

#include <string>
#include <iostream>
#include <fmt/core.h>

using std::cout; using std::endl;
using std::string;

class Character {

protected:
   // Name of the character
   string name;
   // Maximum (and initial) number of HP
   size_t maxHP;
   // Current number of HP
   size_t hitPoints;

public:
   // Base constructor
   Character(string name){
      Character::name = name;
   };

   static size_t nPlayers(){
      static int alive;
      return alive;
   };  // Return the number of players that are not dead (ie HP=0)

   string to_string() const;  // Convert into a string

   bool isAlive() const{
      if(hitPoints > 0){
         return 1;
      } else return 0;  // Return if the player is still alive (ie HP>0)
   }
   void changeHP(int delta){
      hitPoints = hitPoints + delta;
      if(hitPoints <= 0){
         hitPoints = 0;
      } else if(hitPoints >= maxHP){
         hitPoints = maxHP;
      }
   };  // Change the number of HP of the character (positive of negative delta)
};

class Warrior: public Character {
private:
   // Number of HP remove after an attack
   size_t attackPts;

public:
// Base constructor
   Warrior(string name, size_t attackPts);

   size_t maxHP = 15;
   size_t hitPoints = maxHP;

   string to_string() const{
      string res = "";
      string temphitPoints = fmt::format("{:.0}", hitPoints);
      string tempmaxHP = fmt::format("{:.0}", maxHP);
      string tempattackPts = fmt::format("{:.0}", attackPts);
      res = name + " (warrior, HP:" + temphitPoints + "/" + tempmaxHP + ", atk:" + tempattackPts + ")";
      return res;
   }; // Convert into a string

   void attacks(Character& other){
      other.changeHP(-attackPts);
   };  // Attack an other character
};

class Healer: public Character {
private:
   // Number of HP added after heal
   size_t healPts;

public:
// Base constructor
   Healer(string name, size_t healPts);
   Character(name);

   size_t maxHP = 8;
   size_t hitPoints = maxHP;

   string to_string() const{
      string res = "";
      string temphitPoints = fmt::format("{:.0}", hitPoints);
      string tempmaxHP = fmt::format("{:.0}", maxHP);
      string temphealPts = fmt::format("{:.0}", healPts);
      res = name + " (healer, HP:" + temphitPoints + "/" + tempmaxHP + ", atk:" + temphealPts + ")";
      return res;
   };  // Convert into a string
   void heals(Character& other){
      other.changeHP(healPts);
   };  // Heals an other character
};

#endif
